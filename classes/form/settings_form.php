<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Form for settings
 *
 * @package   plagiarism_proctorio
 * @copyright 2023 onwards Osvaldo Arriola <osvaldo@e-abclearning.com>
 * @author    Miguel Magdalena
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/lib/formslib.php');

/**
 * Class plagiarism_setup_form
 */
class plagiarism_proctorio_settings_form extends moodleform {

    /**
     * Form definition to deploy in plagiarism_proctorio settigns.
     */
    public function definition () {
        global $CFG;

        $mform =& $this->_form;

        // Select common options.
        $ynoptions = array( 0 => get_string('no'), 1 => get_string('yes'));

        // Enabled Proctorio.
        $mform->addElement('select', 'enabled', get_string("enabled", "plagiarism_proctorio"), $ynoptions);

        // Student Disclosure.
        $mform->addElement('textarea',
            'student_disclosure',
            get_string('setting_student_disclosure', 'plagiarism_proctorio'),
            'wrap="virtual" rows="6" cols="50"'
        );
        $mform->setDefault('student_disclosure', get_string('setting_student_disclosure_default', 'plagiarism_proctorio'));
        $mform->addHelpButton('student_disclosure', 'setting_student_disclosure', 'plagiarism_proctorio');

        $this->add_action_buttons(true);
    }
}
