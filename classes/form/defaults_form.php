<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Contains class plagiarism_proctorio_defaults_form
 *
 * @package   plagiarism_proctorio
 * @copyright 2023 Osvaldo Arriola <osvaldo@e-abclearning.com>
 * @author    Miguel Magdalena
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/lib/formslib.php');
require_once(dirname(__FILE__, 3) . '/locallib.php');

/**
 * Class plagiarism_proctorio_defaults_form
 *
 */
class plagiarism_proctorio_defaults_form extends moodleform {
    /**
     * Form definition to deploy in plagiarism_proctorio default settigns tab.
     */
    public function definition () {
        $mform =& $this->_form;

        // Select common options.
        $ynoptions = array( 0 => get_string('no'), 1 => get_string('yes'));
        $tiioptions = array(
            0 => get_string("never"),
            1 => get_string("always"),
            2 => get_string("showwhendue", "plagiarism_proctorio"),
            3 => get_string("showwhencutoff", "plagiarism_proctorio")
        );
        $draftoptions = array(
            DRAFTSUBMIT_IMMEDIATE => get_string("submitondraft", "plagiarism_proctorio"),
            DRAFTSUBMIT_FINAL => get_string("submitonfinal", "plagiarism_proctorio")
        );
        $contentoptions = array(
            RESTRICTCONTENTNO => get_string('restrictcontentno', 'plagiarism_proctorio'),
            RESTRICTCONTENTFILES => get_string('restrictcontentfiles', 'plagiarism_proctorio'),
            RESTRICTCONTENTTEXT => get_string('restrictcontenttext', 'plagiarism_proctorio')
        );
        $filetypes = proctorio_default_allowed_file_types(true);
        $supportedfiles = array();
        foreach ($filetypes as $ext => $mime) {
            $supportedfiles[$ext] = $ext;
        }

        // List of modules supported by this plugin.
        $supportedmodules = proctorio_supported_modules();

        foreach ($supportedmodules as $sm) {

            // Check support of plagiarism in core.
            if (!plugin_supports('mod', $sm, FEATURE_PLAGIARISM)) {
                continue;
            }

            // Header for this module.
            $mform->addElement('header', 'title_'.$sm, get_string('defaults_'.$sm, 'plagiarism_proctorio'));

            // Option for enable this module.
            $mform->addElement('select', 'enabled_module_'.$sm, get_string("enabled", "plagiarism_proctorio"), $ynoptions);

            // Option for show student score in this module.
            $mform->addElement('select', 'show_student_score_'.$sm,
                get_string("show_student_score", "plagiarism_proctorio"), $tiioptions);
            $mform->addHelpButton('show_student_score_'.$sm, 'show_student_score', 'plagiarism_proctorio');

            // Option for show student report.
            $mform->addElement('select', 'show_student_report_'.$sm,
                get_string("show_student_report", "plagiarism_proctorio"), $tiioptions);
            $mform->addHelpButton('show_student_report_'.$sm, 'show_student_report', 'plagiarism_proctorio');

            // Assign specific options.
            if ($sm == 'assign') {
                // Option for know when should the file be submitted.
                $mform->addElement('select', 'draft_submit_'.$sm,
                    get_string("draft_submit", "plagiarism_proctorio"), $draftoptions);
            }

            // Option for restrict content.
            $mform->addElement('select', 'restrictcontent_'.$sm,
                get_string('restrictcontent', 'plagiarism_proctorio'), $contentoptions);
            $mform->addHelpButton('restrictcontent_'.$sm, 'restrictcontent', 'plagiarism_proctorio');
            $mform->setType('restrictcontent_'.$sm, PARAM_INT);

            // Option for type receiver email.
            $mform->addElement('text', 'receiver_' . $sm,
                get_string("receiver", "plagiarism_proctorio"), array('size' => 40));
            $mform->addHelpButton('receiver_' . $sm, 'receiver', 'plagiarism_proctorio');
            $mform->setType('receiver_' . $sm, PARAM_EMAIL);

            // Option for sent email to student.
            $mform->addElement('select', 'studentemail_'.$sm,
                get_string("studentemail", "plagiarism_proctorio"), $ynoptions);
            $mform->addHelpButton('studentemail_'.$sm, 'studentemail', 'plagiarism_proctorio');
            $mform->setType('studentemail_'.$sm, PARAM_INT);

            // Option for use all filetypes to use in this module.
            $mform->addElement('select', 'allowallfile_'.$sm,
                get_string('allowallsupportedfiles', 'plagiarism_proctorio'), $ynoptions);
            $mform->addHelpButton('allowallfile_'.$sm, 'allowallsupportedfiles', 'plagiarism_proctorio');
            $mform->setType('allowallfile_'.$sm, PARAM_INT);

            // Option for choose filetypes to use in this module.
            $mform->addElement('select', 'selectfiletypes_'.$sm,
                get_string('restrictfiles', 'plagiarism_proctorio'),
                $supportedfiles, array('multiple' => true));
            $mform->setType('selectfiletypes_'.$sm, PARAM_TAGLIST);

            // Option for save documents in Proctorio database.
            $mform->addElement('select', 'storedocuments_'.$sm,
                get_string('storedocuments', 'plagiarism_proctorio'), $ynoptions);
            $mform->addHelpButton('storedocuments_'.$sm, 'storedocuments', 'plagiarism_proctorio');
            $mform->setType('storedocuments_'.$sm, PARAM_INT);
        }

        $this->add_action_buttons(true);
    }
}
