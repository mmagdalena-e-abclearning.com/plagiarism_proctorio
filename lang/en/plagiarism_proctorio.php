<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package   plagiarism_proctorio
 * @copyright 2023 Osvaldo Arriola <osvaldo@e-abclearning.com>
 * @author    Miguel Magdalena
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname']  = 'Plagiarism Proctorio';
$string['setting_proctorio_explain'] = 'For more information on this plugin see: Proctorio.com';
$string['enabled'] = 'Enable proctorio';
$string['setting_student_disclosure'] = 'Student Disclosure';
$string['setting_student_disclosure_help'] = 'This text will be displayed to all students on the file upload page.';
$string['setting_student_disclosure_default'] = 'All files uploaded will be submitted to a plagiarism detection service';
$string['proctorio'] = 'proctorio template plagiarism plugin';
$string['saved_config_success'] = 'Plagiarism Proctorio Settings Saved';
$string['defaults'] = 'Proctorio defaults';
$string['defaults_assign'] = 'Default assign settings';
$string['defaults_forum'] = 'Default forum settings';
$string['defaults_workshop'] = 'Default workshop settings';
$string['defaults_hsuforum'] = 'Default hsuforum settings';
$string['defaults_quiz'] = 'Default quiz settings';
$string['show_student_score'] = 'Show similarity score to student';
$string['show_student_score_help'] = 'The similarity score is the percentage of the submission that has been matched with other content.';
$string['show_student_report'] = 'Show similarity report to student';
$string['show_student_report_help'] = 'The similarity report gives a breakdown on what parts of the submission were plagiarised and the location that Proctorio first saw this content';
$string['draft_submit'] = 'When should the file be submitted';
$string['restrictcontent'] = 'Submit attached files and in-line text';
$string['restrictcontent_help'] = 'You can decide which components to send to Proctorio: uploaded files and in-line text from forum posts and text from the online text assignment submission type. ';
$string['receiver'] = 'Receiver address';
$string['receiver_help'] = 'This is the unique address for the teacher reacive the report';
$string['studentemail'] = 'Send student email';
$string['studentemail_help'] = 'This will send an e-mail to the student when a file has been processed to let them know that a report is available, the e-mail also includes the opt-out link.';
$string['allowallsupportedfiles'] = 'Allow all supported file types';
$string['allowallsupportedfiles_help'] = 'This allows the teacher to restrict which file types will be sent to Proctorio for processing. It does not prevent students from uploading different file types to the assignment.';
$string['restrictfiles'] = 'File types to submit';
$string['storedocuments'] = 'Add submissions to Proctorio database';
$string['storedocuments_help'] = 'If set to yes, submissions will be added to the Proctorio database for future comparison with other submissions, if set to No the document will be deleted from Proctorio after anaylsis is complete.';
$string['restrictcontentno'] = 'Submit everything';
$string['restrictcontentfiles'] = 'Only submit attached files';
$string['restrictcontenttext'] = 'Only submit in-line text';
$string['submitondraft'] = 'Submit file when first uploaded';
$string['submitonfinal'] = 'Submit file when student sends for marking';
$string['showwhencutoff'] = 'After activity cut off date';
$string['showwhendue'] = 'After activity due date';
$string['defaultsdesc'] = 'The following settings are the defaults set for each Activity Module';
