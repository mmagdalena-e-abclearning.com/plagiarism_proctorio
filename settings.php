<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Admin settings and defaults
 *
 * @package   plagiarism_proctorio
 * @copyright 2023 onwards Osvaldo Arriola <osvaldo@e-abclearning.com>
 * @author    Miguel Magdalena
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(dirname(__FILE__)) . '/../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once(__DIR__.'/classes/form/settings_form.php');
require_login();

$context = context_system::instance();
require_capability('moodle/site:config', $context, $USER->id, true, "nopermissions");

admin_externalpage_setup('plagiarismproctorio');

$mform = new plagiarism_proctorio_settings_form();

if ($mform->is_cancelled()) {
    redirect($CFG->wwwroot);
}

if (($data = $mform->get_data()) && confirm_sesskey()) {
    foreach ($data as $field => $value) {
        if ($field != 'submitbutton') {
            set_config($field, $value, 'plagiarism_proctorio');
        }
    }
    \core\notification::add(get_string('saved_config_success', 'plagiarism_proctorio'), \core\notification::SUCCESS);

}

// Set actual config to form.
$settings = (array)get_config('plagiarism_proctorio');
$mform->set_data($settings);

echo $OUTPUT->header();

// Print Configuration Tabs.
$currenttab = 'proctorio_settings';
require_once('proctorio_tabs.php');

// Print settings form.
echo $OUTPUT->box(get_string('setting_proctorio_explain', 'plagiarism_proctorio'));
$mform->display();

echo $OUTPUT->footer();
