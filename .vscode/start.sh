#!/bin/bash

# Global variables
export COMPOSE_PROJECT_NAME=moodle-412-plagiarism
export MOODLE_DOCKER_WEB_PORT=5001
export MOODLE_DOCKER_PHP_VERSION=7.4
export MOODLE_DOCKER_DB=mariadb
export MOODLE_DOCKER_WWWROOT="/home/miguel/work/moodle-sources/moodle-sources-for-dev/moodle-4.1.2-plagiarism"

# Copy config
cp /home/miguel/work/docker/config.docker-template.php $MOODLE_DOCKER_WWWROOT/config.php

# Copy local.yml
cp /mnt/e/TR-EABCLEARNING/Moodle/plugins/plagiarism_proctorio/.vscode/local.yml /home/miguel/work/docker/local.yml

/home/miguel/work/docker/bin/moodle-docker-wait-for-db

# Start dockers with these config
/home/miguel/work/docker/bin/moodle-docker-compose up -d