#!/bin/bash

# Global variables
export COMPOSE_PROJECT_NAME=moodle-412-plagiarism
export MOODLE_DOCKER_WEB_PORT=5001
export MOODLE_DOCKER_PHP_VERSION=7.4
export MOODLE_DOCKER_DB=mariadb
export MOODLE_DOCKER_WWWROOT="/home/miguel/work/moodle-sources/moodle-sources-for-dev/moodle-latest-412-plagiarism"

# Stop Docker
/home/miguel/work/docker/bin/moodle-docker-compose stop

# Delete local.yml
rm /home/miguel/work/docker/local.yml