<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Related functions
 *
 * @package   plagiarism_proctorio
 * @copyright 2023 Osvaldo Arriola <osvaldo@e-abclearning.com>
 * @author    Miguel Magdalena
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;
global $CFG;
require_once($CFG->dirroot.'/plagiarism/lib.php');
require_once($CFG->dirroot . '/lib/filelib.php');

/**
 * DRAFTSUBMIT_IMMEDIATE - Assing module: Submit file when first uploaded.
 */
define('DRAFTSUBMIT_IMMEDIATE', 0);

/**
 * DRAFTSUBMIT_FINAL - Assing module: Submit file when student sends for marking.
 */
define('DRAFTSUBMIT_FINAL', 1);

/**
 * RESTRICTCONTENTNO - Submit everything (inline-text and  file attachments).
 */
define('RESTRICTCONTENTNO', 0);

/**
 * RESTRICTCONTENTFILES - Only submit attached files.
 */
define('RESTRICTCONTENTFILES', 1);

/**
 * RESTRICTCONTENTTEXT - Only submit in-line text.
 */
define('RESTRICTCONTENTTEXT', 2);

/**
 * Function to list plugins supported.
 *
 * @return array of supported modules
 */
function proctorio_supported_modules() {
    $supportedmodules = array('assign', 'forum', 'workshop', 'quiz');
    return $supportedmodules;
}

/**
 * Used to get allowed file types
 *
 * @return array of file types
 */
function proctorio_default_allowed_file_types() {

    $filetypes = array('doc'  => 'application/msword',
        'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'sxw'  => 'application/vnd.sun.xml.writer',
        'pdf'  => 'application/pdf',
        'txt'  => 'text/plain',
        'rtf'  => 'application/rtf',
        'html' => 'text/html',
        'htm'  => 'text/html',
        'wps'  => 'application/vnd.ms-works',
        'odt'  => 'application/vnd.oasis.opendocument.text',
        'pages' => 'application/x-iwork-pages-sffpages',
        'xls' => 'application/vnd.ms-excel',
        'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'ps' => 'application/postscript',
        'hwp' => 'application/x-hwp');

    return $filetypes;

}
